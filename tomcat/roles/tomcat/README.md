tomcat playbook
==============
This playbook installs tomcat
Restricted and simplified to demover
Requirements
------------
### Supported Operating Systems

- RedHat-family Linux Distributions

Usage
-----
Create `tomcat.yml` playbook and execute it:

```---
- hosts: all
  user: "{{ ansible_ssh_user }}"

  roles:
    - tomcat
```
