generic playbook
==============
This playbook prepares server's workspace
Restricted to demo ver

Requirements
------------
### Supported Operating Systems

- RedHat-family Linux Distributions

Usage
-----
Create `generic.yml` playbook and execute it:

```---
- hosts: all
  user: "{{ ansible_ssh_user }}"

  roles:
    - generic
```
