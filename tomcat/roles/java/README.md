java playbook
==============
This playbook installs java via yum
Very simpliefied demo ver

Requirements
------------
### Supported Operating Systems

- RedHat-family Linux Distributions

Attributes
----------
### Recommended tunables
java_packages, def in default

Tags
----------
### Supported tags

- `java_install`
  - Download and install java

Usage
-----
Create `java.yml` playbook and execute it:

```---
- hosts: all
  user: "{{ ansible_ssh_user }}"

  roles:
    - java
```
