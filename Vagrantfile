# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.require_version ">= 1.8.0"
vm_ip = '192.168.17.78'
project_key = 'demo'

Vagrant.configure(2) do |config|

  config.vm.hostname = "#{project_key}.dev"

  config.vm.box_url = 'https://atlas.hashicorp.com/elegoev/boxes/vagrant-centos-7.1-docker-compose/versions/1.2.7/providers/virtualbox.box'
#  config.vm.box_url = 'file://./virtualbox.box'
  config.vm.box = "centos67-docker"
  config.vm.post_up_message = "Welcome to #{project_key}!\n Your env parts are available at IP: #{vm_ip}"
  config.ssh.insert_key = false

  config.vm.provider :virtualbox do |vb|
    vb.gui = false
    vb.customize ["modifyvm", :id, "--cpus", 2]
    vb.customize ["modifyvm", :id, "--memory", 2048]
    vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
  end

  config.vm.network :private_network, ip: vm_ip

  config.vm.provider "vmware_fusion" do |v|
    v.vmx["memsize"] = "2048"
    v.vmx["numvcpus"] = 2
    v.vmx["ethernet0.virtualDev"] = "vmxnet3"
  end
#downgraded ansible to 1.9 due common problem with ansible2 and vagrant1.8.1 until  vagrant 1.8.2
  config.vm.provision "install Ansible",
    inline: "yum install -y epel-release && yum install -y ansible1.9",
    run: "once",
    privileged: true,
    type: 'shell'

  config.vm.provision "generate RSA keys for vagrant",
    inline: 'ssh-keygen -t RSA -N "" -f ~/.ssh/id_rsa && touch ~/.ssh/known_hosts',
    run: "once",
    privileged: false,
    type: 'shell'


  config.vm.provision "Create docker default network",
    inline: "docker network create docker_default",
    run: "once",
    privileged: true,
    type: 'shell'

  config.vm.provision "Build containers for web and tomcat",
    inline: "cd /vagrant/docker && /usr/local/bin/docker-compose up -d",
    run: "once",
    type: 'shell'


  config.vm.provision "Show created containers for web and tomcat",
    inline: "sleep 5 && cd /vagrant/docker && /usr/local/bin/docker-compose ps",
    run: "once",
    type: 'shell'

  config.vm.provision "Generate ssh keys and grant access to web and tmc",
    inline: 'ssh-keygen -R 172.18.0.2 && ssh-keygen -R 172.18.0.3 && ssh-keyscan -H 172.18.0.2 >> ~/.ssh/known_hosts && ssh-keyscan -H 172.18.0.3 >> ~/.ssh/known_hosts && sshpass -p demo ssh-copy-id demo@172.18.0.2 && sshpass -p demo ssh-copy-id demo@172.18.0.3',
    run: "once",
    privileged: false,
    type: 'shell'

    config.vm.provision 'tomcat playbook', type: 'ansible_local', run: "once" do |ansible|
	ansible.verbose = 'vvvv'
	ansible.limit = 'all'
	ansible.sudo = false
	ansible.playbook = '/vagrant/tomcat/setup.yml'
	ansible.inventory_path = '/vagrant/tomcat/inv/myinv'
    end

    config.vm.provision 'nginx playbook', type: 'ansible_local', run: "once" do |ansible|
	ansible.verbose = 'vvvv'
	ansible.limit = 'all'
	ansible.sudo = false
	ansible.playbook = '/vagrant/tomcat/nginx.yml'
	ansible.inventory_path = '/vagrant/tomcat/inv/myinv'
    end
    
end