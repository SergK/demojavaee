# **This is JavaEE tools demo project**
#### *;-) Java is a job, C/linux/kernel/systemprg is a hobby!*
---
## 1. **Architecture scheme:**
- **System:** Vagrantbox -> docker-compose:docker_containers <- Ansible

- **Application design:** Web app = Nginx + tomcat_javaservlet

## 2. **Getting Started ;-)**
 - **Prerequisites**
    -  Vagrant and VirtualBox should be installed in your system!
    - Full access Internet, if you have any restrictions like proxy,firewalls - it is up to you to resolve this
    - ;-) Understanding what and why you are doing
    
 - **Lets do it! step by step start:**

    ```
    $ git clone git@gitlab.com:SergK/demojavaee.git
    ```
    
    ```
    $ cd demojavaee
    ```
    
    ```
    $ vagrant up
    ```
    
    Open your fav browser and type: http://192.168.17.78:8080
   
    